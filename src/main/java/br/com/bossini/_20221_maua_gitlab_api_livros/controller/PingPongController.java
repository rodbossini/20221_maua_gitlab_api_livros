package br.com.bossini._20221_maua_gitlab_api_livros.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//https://host.com.br/ping
@RestController
@RequestMapping ("/ping")
public class PingPongController {

    @GetMapping
    public String executarTeste (){
        return "pong";
    }
}
