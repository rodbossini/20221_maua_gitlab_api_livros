package br.com.bossini._20221_maua_gitlab_api_livros.controller;

import br.com.bossini._20221_maua_gitlab_api_livros.model.Livro;
import br.com.bossini._20221_maua_gitlab_api_livros.service.LivroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//https://host.com.br/livros
//app.post ('/livros', (req, res) => {
//
// })
//https://host.com.br/livros
@RestController
@RequestMapping ("/livros")
public class LivroController {

    @Autowired
    private LivroService livroService;

    //https://host.com.br/livros/salvar
    @PostMapping("/salvar")
    public void salvar (@RequestBody Livro livro){
        livroService.salvar(livro);
    }

    //https://host.com.br/livros
    @GetMapping
    public List <Livro> listar (){
        return livroService.listar();
    }
}
